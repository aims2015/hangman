package com.example.lynn.hangman;

import android.view.View;
import android.widget.Button;

import static com.example.lynn.hangman.MainActivity.*;

/**
 * Created by lynn on 6/28/2015.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button source = (Button) v;

            if (source == restart) {
                wordView.setup();

                for (int counter=0;counter<alphabet.length;counter++)
                    alphabet[counter].setText(String.valueOf((char)(65 + counter)));

                head = false;
                leftArm = false;
                rightArm = false;
                body = false;
                leftLeg = false;
                rightLeg = false;

                next = 1;

                hangmanView.invalidate();

                message.setText("");
            } else {
                char letter = source.getText().toString().charAt(0);

                boolean found = false;

                boolean uncovered = true;

                for (int counter = 0; counter < buttons.length; counter++) {
                    if (letter == buttons[counter].getTag().toString().charAt(0)) {
                        buttons[counter].setText(String.valueOf(letter));

                        found = true;
                    }

                    uncovered = uncovered && !buttons[counter].getText().toString().equals("");
                }

                source.setText("");

                if (!found) {
                    switch (next) {
                        case 6:
                            rightLeg = true;
                        case 5:
                            leftLeg = true;
                        case 4:
                            body = true;
                        case 3:
                            rightArm = true;
                        case 2:
                            leftArm = true;
                        case 1:
                            head = true;
                    }

                    hangmanView.invalidate();

                    next++;

                    if (next == 7) {
                        restart.setVisibility(View.VISIBLE);

                        message.setText("You Lose");

                        for (int counter = 0; counter < buttons.length; counter++)
                            buttons[counter].setText(buttons[counter].getTag().toString());
                    }
                }

                if (uncovered) {
                    restart.setVisibility(View.VISIBLE);

                    message.setText("You win");
                }
            }
        }
    }

}
