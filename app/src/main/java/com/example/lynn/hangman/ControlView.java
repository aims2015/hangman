package com.example.lynn.hangman;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.example.lynn.hangman.MainActivity.*;

/**
 * Created by lynn on 6/29/2015.
 */
public class ControlView extends LinearLayout {

    public ControlView(Context context) {
        super(context);

        restart = new Button(context);

        restart.setText("Restart");

        restart.setVisibility(View.INVISIBLE);

        restart.setId(R.id.restart);

        restart.setOnClickListener(listener);

        addView(restart);

        message = new TextView(context);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.restart);

        message.setLayoutParams(layoutParams);

        addView(message);
    }

}
