package com.example.lynn.hangman;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    public static boolean head;
    public static boolean leftArm;
    public static boolean rightArm;
    public static boolean body;
    public static boolean leftLeg;
    public static boolean rightLeg;
    public static HangmanView hangmanView;
    public static Button[] alphabet;
    public static AlphabetView alphabetView;
    public static Button[] buttons;
    public static WordView wordView;
    public static MyDatabaseHelper helper;
    public static String word;
    public static MyListener listener;
    public static int next;
    public static Button restart;
    public static ControlView controlView;
    public static TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = new MyDatabaseHelper(this);

        listener = new MyListener();

        next = 1;

        setContentView(new MyView(this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
