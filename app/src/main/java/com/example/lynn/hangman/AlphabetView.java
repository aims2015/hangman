package com.example.lynn.hangman;

import android.content.Context;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

import static com.example.lynn.hangman.MainActivity.*;

/**
 * Created by lynn on 6/28/2015.
 */
public class AlphabetView extends TableLayout {

    public AlphabetView(Context context) {
        super(context);

        TableRow row = new TableRow(context);

        alphabet = new Button[26];

        for (int counter=0;counter<13;counter++) {
            row.addView(alphabet[counter] = new Button(context));

            alphabet[counter].setText(String.valueOf((char)(counter+65)));
        }

        addView(row);

        row = new TableRow(context);

        for (int counter=13;counter<26;counter++) {
            row.addView(alphabet[counter] = new Button(context));

            alphabet[counter].setText(String.valueOf((char)(counter+65)));
        }

        for (int counter=0;counter<alphabet.length;counter++)
            alphabet[counter].setOnClickListener(listener);

        addView(row);
    }

}
