package com.example.lynn.hangman;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import static com.example.lynn.hangman.MainActivity.*;

/**
 * Created by lynn on 6/28/2015.
 */
public class HangmanView extends View {
    private Paint paint;

    public HangmanView(Context context) {
        super(context);

        paint = new Paint();

        paint.setColor(0xFF000000);
    }

    public void onDraw(Canvas canvas) {
        if (head) {
            paint.setStyle(Paint.Style.STROKE);

            canvas.drawCircle(100, 100, 50, paint);

            canvas.drawLine(100,150,100,170,paint);
        }

        if (leftArm)
            canvas.drawLine(100,170,70,200,paint);

        if (rightArm)
            canvas.drawLine(100,170,130,200,paint);

        if (body)
            canvas.drawLine(100,170,100,300,paint);

        if (leftLeg)
            canvas.drawLine(100,300,70,330,paint);

        if (rightLeg)
            canvas.drawLine(100,300,130,330,paint);
    }

}
