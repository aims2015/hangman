package com.example.lynn.hangman;

import android.content.Context;
import android.widget.RelativeLayout;

import static com.example.lynn.hangman.MainActivity.*;

/**
 * Created by lynn on 6/28/2015.
 */
public class MyView extends RelativeLayout {
    public MyView(Context context) {
        super(context);

        wordView = new WordView(context);

        wordView.setId(R.id.wordview);

        addView(wordView);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.wordview);

        alphabetView = new AlphabetView(context);

        alphabetView.setId(R.id.alphabetview);

        alphabetView.setLayoutParams(layoutParams);

        addView(alphabetView);

        controlView = new ControlView(context);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.alphabetview);

        controlView.setLayoutParams(layoutParams);

        controlView.setId(R.id.controlview);

        addView(controlView);

        hangmanView = new HangmanView(context);

        hangmanView.setId(R.id.hangmanview);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.wordview);
        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.alphabetview);
      //  layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.controlview);

        hangmanView.setLayoutParams(layoutParams);

        addView(hangmanView);

    }
}
