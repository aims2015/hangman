package com.example.lynn.hangman;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

import static com.example.lynn.hangman.MainActivity.*;

/**
 * Created by lynn on 6/28/2015.
 */
public class WordView extends LinearLayout {

    public String getWord() {
        SQLiteDatabase database = helper.getReadableDatabase();

        ArrayList<String> words = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM hangmanwords;",new String[]{});

        cursor.moveToFirst();

        do {
            int wordIndex = cursor.getColumnIndex("word");

            String word = cursor.getString(wordIndex);

            words.add(word.toUpperCase());
        } while (cursor.moveToNext());

        String word = words.get((int)(words.size()*Math.random()));


        return(word);
    }

    public WordView(Context context) {
        super(context);

        setup();
    }

    public void setup() {
        word = getWord();

        buttons = new Button[word.length()];

        removeAllViews();

        for (int counter=0;counter<buttons.length;counter++) {
            buttons[counter] = new Button(getContext());

            buttons[counter].setTag(word.substring(counter,counter+1));

            addView(buttons[counter]);
        }

        invalidate();
    }

}
